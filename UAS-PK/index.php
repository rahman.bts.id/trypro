<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.tailwindcss.com"></script>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <script src="script.js"></script>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Outfit:wght@600&display=swap" rel="stylesheet">
    <title>Pemilihan Presiden</title>
</head>
<body style="font-family: 'Outfit', sans-serif;" class="flex border flex-col w-screen h-screen justify-center item-center p-20">
    <header class="p-4">
        <h1 class="text-lg border-b p-2">Pendataan Suara Hasil Pilpres</h1>
    </header>
    <main class="p-4">
        <form id="pemilihanForm" class="flex flex-col border gap-2 p-2">
            <div class="flex flex-row gap-2">
                <label class="w-48 flex h-10 items-center" for="provinsi">Provinsi <span class="text-red-600">*</span></label>
                <select class="border w-96 rounded p-2" id="provinsi" name="provinsi">
                    <option disabled value="">Pilih satu</option>
                </select>
            </div>
            <div class="flex flex-row gap-2">
                <label class="w-48 flex h-10 items-center" for="calon">Calon Presiden<span class="text-red-600">*</span></label>
                <select class="border w-96 rounded p-2" id="calon" name="calon"></select>
            </div>
            <div class="flex flex-row gap-2">
                <label class="w-48 flex h-10 items-center" for="perolehanSuara">Perolehan Suara<span class="text-red-600">*</span></label>
                <input class="border p-2 w-80 rounded" type="number" id="perolehanSuara" name="perolehanSuara" placeholder="Cth:2075">
                <span class="border flex items-center justify-center w-16 rounded">Suara</span>
            </div>
            <div class="flex flex-row justify-end">
                <button type="button" class="bg-blue-600 w-20 h-8 rounded text-white font-bold" onclick="kirimForm()">Kirim</button>
            </div>
        </form>
    </main>
    <h2 class="pl-4">Result</h2>
    <div class="p-4 border ml-4 h-16 flex item-center rounded" id="hasilPemrosesan"></div>

</body>
</html>
