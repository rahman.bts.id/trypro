<?php
class Pemilu {
    public $provinsi;
    public $calon;
    public $suara;
    function __construct($provinsi,$calon,$suara)
    {
        $this->provinsi = $provinsi;
        $this->calon = $calon;
        $this->suara = $suara;
    }
}

function getData (){

    $provinsi = $_POST['provinsi'];
    $calon = $_POST['calon'];
    $suara = $_POST['perolehanSuara'];
    $result = new Pemilu($provinsi,$calon,$suara);

    return json_encode($result);
}
echo getData()
?>
