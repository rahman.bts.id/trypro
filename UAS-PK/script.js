$(document).ready(function() {
    // Mengisi Combo Provinsi dari data array
    getProvinsi()
    getCapres()
    // Mengisi Combo Calon Presiden dari data array
   
});
const API_KEY = '6ea80d77101524bb719cf8ab2440f1d56399d44532223b69b2a233698ab46440'

const getProvinsi = async ()=>{
    try {
        const data = await fetch(`https://api.binderbyte.com/wilayah/provinsi?api_key=${API_KEY}`)
        const res = await data.json();
        const result = res.value;
        let provinsiSelect = $("#provinsi");
        result.forEach((provinsi)=> {
            provinsiSelect.append("<option value='" + provinsi.name + "'>" + provinsi.name + "</option>");
        });

    } catch (error) {
        
        console.log(error);
    }
}
const getCapres = ()=>{
    let calonArray = ["Calon 1", "Calon 2", "Calon 3"];
    let calonSelect = $("#calon");
    calonArray.forEach(function(calon) {
        calonSelect.append("<option value='" + calon + "'>" + calon + "</option>");
    });
}

function kirimForm() {
    // Mengambil data dari form
    let provinsi = $("#provinsi").val();
    let calon = $("#calon").val();
    let perolehanSuara = $("#perolehanSuara").val();
    const ajax = new XMLHttpRequest()

    // Mengirim data ke server menggunakan Ajax
    $.ajax({
        url: "proses.php",
        type: "POST",
        data: {
            provinsi: provinsi,
            calon: calon,
            perolehanSuara: perolehanSuara
        },
        success: function(response) {
            console.log(response);
            // Menampilkan hasil pemrosesan data di dalam area HTML
            $("#hasilPemrosesan").html(`Result : ${response}`)
        },
        error: function(error) {
            console.log("Error:", error);
        }
    });
}
